﻿using UnityEngine;
using System.Collections;

public class CAPI : MonoBehaviour {

    public  static GameObject getPrifab(string name) {
        //return Instantiate(Resources.Load(name, typeof(GameObject))) as GameObject;
        
        GameObject myObj = Resources.Load(name) as GameObject;
        if (myObj != null) { 
            return Instantiate<GameObject>(myObj);
        }
        return null;
    }
}
