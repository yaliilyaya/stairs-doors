﻿using UnityEngine;
using System.Collections;

public class Cell_ctrlr : MonoBehaviour {

    public MeshRenderer up_glow_rndr;
    public MeshRenderer down_glow_rndr;

    public bool up_glow_blue = false;
    public bool up_glow_red = false;
    public bool down_glow = false;

    private Color transp_clr;

	// Use this for initialization
	void Start () {
        up_glow_rndr.enabled = true;
        down_glow_rndr.enabled = true;
        transp_clr = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        up_glow_rndr.material.SetColor("_TintColor", transp_clr);
        down_glow_rndr.material.SetColor("_TintColor", transp_clr);
	}
	
	// Update is called once per frame
	void Update () {
        up_glow_rndr.material.SetColor("_TintColor", transp_clr);
        down_glow_rndr.material.SetColor("_TintColor", transp_clr);

        if (up_glow_blue) {
            up_glow_rndr.material.SetColor("_TintColor", Color.blue);
            up_glow_blue = false;
        }
        if (up_glow_red)
        {
            up_glow_rndr.material.SetColor("_TintColor", Color.red);
            up_glow_red = false;
        }

        if (Cam_ctrlr.out_field) {
            down_glow_rndr.material.SetColor("_TintColor", Color.red);
        }
	}
}
