﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс для преобразовании координат сетки в мировые координаты
/// </summary>
public class TowerGrid : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    /// <summary>
    /// Выдаёт кординаты ячейки в мировом пространтве
    /// </summary>
    /// <param name="poss"></param>
    /// <returns></returns>
    public static Vector3 getPossCell(Vector2 poss) {
        return new Vector3(poss.x, poss.y, poss.x  + poss.y);
    }
    /// <summary>
    /// Выдаёт кординаты ячейки в мировом пространтве
    /// </summary>
    /// <param name="poss"></param>
    /// <returns></returns>
    public static Vector3 getPossCell(Vector3 poss) {
        poss.y = Mathf.Ceil(poss.y);
        poss.x = Mathf.Ceil(poss.x);
        poss.z = poss.x + poss.y;
        return poss;
    }
    /// <summary>
    /// Выдаёт кординаты ячейки в сетке
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="poss"></param>
    /// <returns></returns>
    public static Vector2 getPossCell<T>(Vector3 poss)
    {
        poss.x = Mathf.Ceil(poss.x);
        poss.y = Mathf.Ceil(poss.y);        
        poss.z = poss.x + poss.y;

        return new Vector2(poss.x, poss.y);
    }

    /// <summary>
    /// Выдаёт рядом находящиеся ячейки  
    /// </summary>
    /// <param name="pos">Координаты текущей ячейки</param>
    /// <returns></returns>
    public static Vector2[] getNearCell(Vector2 pos) {
        return new Vector2[6] {
            pos + new Vector2(-1, 1),
            pos + new Vector2(0, 1),
            pos + new Vector2(1, 0),
            pos + new Vector2(1, -1),
            pos + new Vector2(0, -1),
            pos + new Vector2(-1, 0)
        };
    }

}
