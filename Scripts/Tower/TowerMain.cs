﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Класс отвечает за игровой процесс осуществляемый в башне.
/// Игра в башне это аркадная игра правила которой описаны по ссылке
/// link: https://www.youtube.com/watch?v=5ErXu__Vipk
/// 
/// </summary>
public class TowerMain : MonoBehaviour {

    protected List<TowerRoom> room_list = new List<TowerRoom>();
    protected static TowerMain instance = null;
    
    /// <summary>
    /// Используем сигнлтон для доступа к объекту
    /// </summary>
    protected static TowerMain I {
        get { return TowerMain.instance;  }
        private set { TowerMain.instance = value; }
    }
    // Use this for initialization
    void Start () {
        TowerMain.I = this;
        if (false) {// реализует все варианты комнат для постраения уровня
            for (int left = 0; left < 3; left++)
            {
                for (int right = 0; right < 3; right++)
                {
                    for (int bottom = 0; bottom < 3; bottom++)
                    {
                        int number_part = TowerRoom.getNumber(left, right, bottom);
                        TowerRoom.create(right * 3 + bottom - 4, left - 1, number_part);
                    }
                }
            }
        }
        
        this.room_list.Add(TowerRoom.create(-2, 2, 26));
        this.room_list.Add(TowerRoom.create(-2, 1, 29));
        this.room_list.Add(TowerRoom.create(-1, 1, 6));
        this.room_list.Add(TowerRoom.create(0, 0, 3));
        this.room_list.Add(TowerRoom.create(0, -1, 12));
        this.room_list.Add(TowerRoom.create(1, -1, 4));
        this.room_list.Add(TowerRoom.create(1, 0, 8));
        this.room_list.Add(TowerRoom.create(2, 0, 30));
        /* */
        //this.room_list.Add(TowerRoom.create(0, 0, 3));

    }


    // Update is called once per frame
    void Update () {
	
	}

    /// <summary>
    /// Назодит комнату по позиции на сетке
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static TowerRoom findRoom(Vector2 pos)
    {
        return TowerMain.I.room_list.Find(x => x.position == pos);
    }
    /// <summary>
    /// Назодит список комнат по позиции на сетке
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static TowerRoom[] findRoom(Vector2[] pos)
    {
        TowerRoom[] room_list = new TowerRoom[pos.Length];
        for (int i = 0; i < pos.Length; i++) {
            room_list[i] = TowerMain.findRoom(pos[i]);
        }
        return room_list;
    }
    /// <summary>
    /// Ищет рядом находящиеся комнаты
    /// </summary>
    /// <param name="room"></param>
    /// <returns></returns>
    public static List<TowerRoom> findNearRoom(TowerRoom room) {
        if (room == null) {
            return new List<TowerRoom>();
        }     
        Vector2[] near_pos_list = TowerGrid.getNearCell(room.position);
        List<TowerRoom> text_list = new List<TowerRoom>(TowerMain.findRoom(near_pos_list));
        return text_list;
    }
    /// <summary>
    /// Ищет рядом находящиеся комнаты по списку комнат.
    /// Необходимо для реализации проверки на целостность
    /// </summary>
    /// <param name="list_room"></param>
    /// <returns></returns>
    public static List<TowerRoom> findNearRoom(List<TowerRoom> list_room)
    {
        List<TowerRoom> near_room_list = new List<TowerRoom>();

        foreach (TowerRoom room in list_room) {
            near_room_list.AddRange(
                TowerMain.findNearRoom(room)
            );
        }
        return near_room_list;
    }
    /// <summary>
    /// Выдаёт позицию курсора на сетке в мировых координатах
    /// </summary>
    /// <returns></returns>
    public static Vector3 getCursorPosition()
    {
        Vector3 mouse_position = Camera.main.ScreenPointToRay(Input.mousePosition).GetPoint(5f);//позиция курсора от камеры на растоянии 5 юнитов
        mouse_position = Camera.main.transform.InverseTransformPoint(mouse_position);//Инверитруем позицию в мировые координаты

        return Camera.main.transform.TransformPoint(mouse_position);
    }
    /// <summary>
    /// Проверяет целостность структуры исключая поднятую комнату
    /// </summary>
    /// <param name="room"></param>
    /// <returns></returns>
    public static bool isWhotStructure(TowerRoom room) {

        List<TowerRoom> text_list = new List<TowerRoom>() {};
        List<TowerRoom> diff_list = new List<TowerRoom>() { TowerMain.I.room_list.Find(x => x != room) }; //за основу берём любую комнату кроме текущей
        
        while (diff_list.Count > 0) {
            text_list.AddRange(diff_list);  //добавляем в лист различия
            diff_list = TowerMain.findNearRoom(diff_list); //Ищет всё окружение каждой комнаты в списке
            diff_list.RemoveAll(x => x == null || text_list.Exists(y => y == x) || x == room); //исключаем из списка то что уже найдено и текущёю комнату
        }
        //Debug.Log(TowerMain.I.room_list.Count + " => " + text_list.Count );
        //Debug.Log((text_list.Count + 1));

        return (TowerMain.I.room_list.Count <= (text_list.Count + 1)); //Если в списке меньше комнат чем в основном списке структура не цела
        //TODO:: Есть бага когда в списке встречаются дублирующие комнаты
    }















    private static string mess = "";
    void OnGUI()
    {
        GUILayout.Label(TowerMain.mess);
        //TowerMain.mess = "";
    }

    public static void setMess(Vector3 pos) {
        TowerMain.setMess(pos.ToString());
    }
    public static void setMess(Vector2 pos)
    {
        TowerMain.setMess(pos.ToString());
    }
    public static void setMess(string mess) {
        TowerMain.mess = mess;
    }
    
    
}
