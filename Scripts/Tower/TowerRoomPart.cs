﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Клас реализует части комноты со своими проверками
/// типы задаются следующим видом
/// Left number + number * 12 
/// </summary>
public class TowerRoomPart {

    public TowerRoomPartCheck check_obj;
    public GameObject gameObject;

    protected int number = 0;
    protected TowerRoom room = null;
    
    public const int TYPE_LEFT = 1,
                     TYPE_RIGHT = 2,
                     TYPE_BOTTOM = 3;

    public const int TYPE_WALL = 0,
                     TYPE_FLOOR = 0,
                     TYPE_DOOR = 2,
                     TYPE_STAGE = 1,
                     TYPE_STAGE_RIGHT = 1,
                     TYPE_STAGE_LEFT = 2;

    /// <summary>
    /// Создаём Часть комнаты с привязкой к самой комнате
    /// </summary>
    /// <param name="room"></param>
    /// <param name="number"></param>
     public TowerRoomPart(TowerRoom room, int number) { 
        this.room = room;
        this.number = number;

        this.gameObject = this.loadPrefab(number);
        if (this.gameObject != null)
        {
            this.gameObject.transform.parent = this.room.transform;
        }
    }

    /// <summary>
    /// Создаём Часть комнаты с привязкой к самой комнате
    /// </summary>
    /// <param name="room"></param>
    /// <param name="number"></param>
    public static TowerRoomPart create(TowerRoom room, int part_type, int number) {

        switch (part_type)
        {
            case TowerRoomPart.TYPE_LEFT:
                {
                    return new TowerRoomPartLeft(room, number);
                }
            case TowerRoomPart.TYPE_RIGHT:
                {
                    return new TowerRoomPartRight(room, number);
                }
            case TowerRoomPart.TYPE_BOTTOM:
                {
                    return new TowerRoomPartBottom(room, number);
                }
        }
        return null;
    }

    /// <summary>
    /// Метод возвращает список названий прифабов частей комнаты.
    /// Название прифаба привязана к индексу 
    /// </summary>
    /// <returns></returns>
    protected virtual string[] getPrefabName() {
        Debug.Log("default getPrefab");
        return new string[0];
    }
    /// <summary>
    /// Загружает прифаб части комнаты из Resources 
    /// </summary>
    /// <param name="number">Номер части комнаты</param>
    /// <returns>Загруженный обьект части комнаты</returns>
    protected GameObject loadPrefab(int number)
    {
        string[] list = this.getPrefabName();
        return this.loadPrefab(list[number]);
    }
    /// <summary>
    /// Загружает прифаб части комнаты из Resources 
    /// </summary>
    /// <param name="name">Имя прифаба</param>
    /// <returns>Загруженный обьект части комнаты</returns>
    protected GameObject loadPrefab(string name)
    {
        return CAPI.getPrifab("RoomPart/" + name);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public int getNumber() {
        return this.number;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="room"></param>
    public void setRoom(TowerRoom room) {
        this.room = room;
    }
    /// <summary>
    /// Осуществляет проверку на правила стыковки части комнаты
    /// </summary>
    /// <param name="list_near_room"></param>
    /// <returns></returns>
    public virtual bool check(List<TowerRoom> list_near_room) {
        return true;
    }
    /// <summary>
    /// Выдаёт обьект проверки для определённой части комнаты
    /// </summary>
    /// <param name="list_near_room"></param>
    /// <param name="number">номер прифаба</param>
    /// <param name="type">номер стороны</param>
    /// <returns></returns>
    protected TowerRoomPartCheck getCheckObj(List<TowerRoom> list_near_room, int number, int type) {
        TowerRoom room = list_near_room[number];
        return (room != null && room != this.room ? room.getPart(type).check_obj : null);
    }

}
