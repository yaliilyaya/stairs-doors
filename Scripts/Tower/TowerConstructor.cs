﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Класс реализующий логику передвижения комнат 
/// </summary>
public class TowerConstructor : MonoBehaviour {
    RaycastHit hit;
    TowerRoom room;
    GameObject cursor;
    Ray ray;
    // Use this for initialization
    void Start () {
        
        this.cursor = CAPI.getPrifab("TowerConstructCursor");
        this.CursorDisable();
    }

    /// <summary>
    /// Обновляется каждый кадр
    /// </summary>
    void Update()
    {        
        if (Input.GetMouseButtonDown(0))// При нажатии клавиши поднимаем в воздух комнату 
        {
            this.ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))
            {
                this.room = TowerRoom.get(this.hit.collider.gameObject);
                this.hit.collider.enabled = false; // активируем курсор
                this.CursorEnableb();
                //Vector3 pos = this.hit.collider.gameObject.transform.position;
                //this.createRoom(pos);
            }
        }
        if (this.room != null) { //Если комната захвачена то премещаем её вместе с курсором
            
            Vector3 obj_pos = TowerMain.getCursorPosition();
            Vector3 cell_pos = TowerGrid.getPossCell(obj_pos);
            Vector2 cell_pos2 = TowerGrid.getPossCell<Vector2>(cell_pos);
            //TowerMain.setMess(cell_pos2);
            bool access = this.room.check(cell_pos2); //Проверяем правила стыковки комнаты
            this.setCursorType(access ? 1 :2); /


            this.cursor.transform.position = cell_pos;//Курсор должен выделить ту ячейку где находится курсор мыши
            this.room.transform.position = obj_pos;

            if (Input.GetMouseButtonUp(0))
            {
                this.CursorDisable();
                if (access) {                    
                    this.room.setPosition(cell_pos);
                }
                else {
                    this.room.setPosition(this.room.position);
                }
                this.hit.collider.enabled = true;
                this.room = null;
            }
        }        
    }
    /// <summary>
    /// Задаёт цвет курсора в зависит от статуса
    /// </summary>
    /// <param name="type"></param>
    private void setCursorType(int type)
    {
        if (type == 1) {
            this.cursor.GetComponent<MeshRenderer>().materials[0].color = Color.white;
        }
        else if (type == 2) {
            this.cursor.GetComponent<MeshRenderer>().materials[0].color = Color.red;
        } 

        this.cursor.SetActive(true);
    }
    /// <summary>
    /// Показывает курсор 
    /// </summary>
    private void CursorEnableb() {
        this.cursor.SetActive(true);
    }
    /// <summary>
    /// Скрывает курсор
    /// </summary>
    private void CursorDisable()
    {
        this.cursor.SetActive(false);
    }
}
