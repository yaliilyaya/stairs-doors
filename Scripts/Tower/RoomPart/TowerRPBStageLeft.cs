﻿using UnityEngine;
using System.Collections;

public class TowerRPBStageLeft : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPBStageLeft.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPRStage>(check_obj)
            
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPBStageLeft.checkRight " + check_obj.ToString());
        return (
            this.checkType<TowerRPLWall>(check_obj)
            );
    }
}
