﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 
/// </summary>
public class TowerRoomPartBottom : TowerRoomPart
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="room"></param>
    /// <param name="number"></param>
    public TowerRoomPartBottom(TowerRoom room, int number) : base (room, number)
    {
        if (this.gameObject != null)
        {
            this.gameObject.transform.localPosition = new Vector3(0, -1, 0);
        }
        this.check_obj = getCheckObj(number);
        this.check_obj.number = number;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    protected TowerRoomPartCheck getCheckObj(int number)
    {
        switch (number)
        {
            case TowerRoomPart.TYPE_FLOOR:
                {
                    return new TowerRPBFloor();
                }
            case TowerRoomPart.TYPE_STAGE_LEFT:
                {
                    return new TowerRPBStageLeft();
                }
            case TowerRoomPart.TYPE_STAGE_RIGHT:
                {
                    return new TowerRPBStageRight();
                }
        }
        return null;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected override string[] getPrefabName()
    {
        return new string[] {
            "BottomFloor",
            "BottomStageRight",
            "BottomStageLeft"
        };
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="list_near_room"></param>
    /// <returns></returns>
    public override bool check(List<TowerRoom> list_near_room)
    {   
        TowerRoomPartCheck[] check_obj_list = new TowerRoomPartCheck[2] {
            this.getCheckObj(list_near_room, 4, TowerRoomPart.TYPE_RIGHT),
            this.getCheckObj(list_near_room, 3, TowerRoomPart.TYPE_LEFT)            
        };
        return this.check_obj.check(check_obj_list[0], check_obj_list[1]);
    }
}
