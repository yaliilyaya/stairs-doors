﻿using UnityEngine;
using System.Collections;

public class TowerRPRDoor : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPRDoor.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPBFloor>(check_obj)
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPRDoor.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPLDoor>(check_obj)
            
            );
    }
}
