﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerRoomPartLeft : TowerRoomPart
{
    public TowerRoomPartLeft(TowerRoom room, int number) : base(room, number)
    {        
        if (this.gameObject != null) {
            this.gameObject.transform.localPosition = new Vector3(-1, 0, 0);
        }
        this.check_obj = getCheckObj(number);
        this.check_obj.number = number;
    }

    protected TowerRoomPartCheck getCheckObj(int number)
    {
        switch (number)
        {
            case TowerRoomPart.TYPE_DOOR:
                {
                    return new TowerRPLDoor();
                }
            case TowerRoomPart.TYPE_STAGE:
                {
                    return new TowerRPLStage();
                }
            case TowerRoomPart.TYPE_WALL:
                {
                    return new TowerRPLWall();
                }
        }
        return null;
    }

    protected override string[] getPrefabName()
    {
        return new string[] {
            "LeftWall",
            "LeftStage",
            "LeftDoor"
        };
    }

    public override bool check(List<TowerRoom> list_near_room)
    {        
        TowerRoomPartCheck[] check_obj_list = new TowerRoomPartCheck[2] {
            this.getCheckObj(list_near_room, 5, TowerRoomPart.TYPE_RIGHT),
            this.getCheckObj(list_near_room, 0, TowerRoomPart.TYPE_BOTTOM)
        };
        return this.check_obj.check(check_obj_list[0], check_obj_list[1]);
    }



}
