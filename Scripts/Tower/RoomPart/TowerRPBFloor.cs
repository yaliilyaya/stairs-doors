﻿using UnityEngine;
using System.Collections;

public class TowerRPBFloor : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPBFloor.checkLeft " + check_obj.ToString());
        return (
            !this.checkType<TowerRPLStage>(check_obj) &&
            !this.checkType<TowerRPRStage>(check_obj)
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPBFloor.checkRight " + check_obj.ToString());
        return (
            !this.checkType<TowerRPLStage>(check_obj) &&
            !this.checkType<TowerRPRStage>(check_obj)
            );
    }
}
