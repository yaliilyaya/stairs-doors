﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerRoomPartRight : TowerRoomPart
{
    public TowerRoomPartRight(TowerRoom room, int number) : base(room, number)
    {
        if (this.gameObject != null)
        {
            this.gameObject.transform.localPosition = new Vector3(0, 0, 1);
        }
        this.check_obj = getCheckObj(number);
        this.check_obj.number = number;
    }

    protected TowerRoomPartCheck getCheckObj(int number) {
        switch (number)
        {
            case TowerRoomPart.TYPE_DOOR:
                {
                    return new TowerRPRDoor();
                }
            case TowerRoomPart.TYPE_STAGE:
                {
                    return new TowerRPRStage();
                }
            case TowerRoomPart.TYPE_WALL:
                {
                    return new TowerRPRWall();
                }
        }
        return null;
    } 

    protected override string[] getPrefabName()
    {
        return new string[] {
            "RightWall",
            "RightStage",
            "RightDoor"
        };
    }

    public override bool check(List<TowerRoom> list_near_room)
    {
        TowerRoomPartCheck[] check_obj_list = new TowerRoomPartCheck[2] {
            this.getCheckObj(list_near_room, 1, TowerRoomPart.TYPE_BOTTOM),
            this.getCheckObj(list_near_room, 2, TowerRoomPart.TYPE_LEFT)
        };
        return this.check_obj.check(check_obj_list[0], check_obj_list[1]);
    }

}
