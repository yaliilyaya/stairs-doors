﻿using UnityEngine;
using System.Collections;

public class TowerRPLWall : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPLWall.checkLeft " + check_obj.ToString());
        
        return (
           !this.checkType<TowerRPRDoor>(check_obj)
           );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPLWall.checkRight " + check_obj.ToString());
        return (
             !this.checkType<TowerRPBStageRight>(check_obj)
             );
    }
}
