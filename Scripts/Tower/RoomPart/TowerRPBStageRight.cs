﻿using UnityEngine;
using System.Collections;

public class TowerRPBStageRight : TowerRoomPartCheck
{
    
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPBStageRight.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPRWall>(check_obj)
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPBStageRight.checkRight " + check_obj.ToString());
        return (
            this.checkType<TowerRPLStage>(check_obj) 
            );
    }
}
