﻿/// <summary>
/// Реализует основной класс для проверок на стыковки
/// TODO:: Переписать на класс конфиг
/// </summary>
public class TowerRoomPartCheck {

    public int number;
    /// <summary>
    /// Основной метод для проверки стыковок
    /// </summary>
    /// <param name="left_obj"></param>
    /// <param name="right_obj"></param>
    /// <returns></returns>
    public bool check(TowerRoomPartCheck left_obj, TowerRoomPartCheck right_obj)
    {
        return (
            (left_obj == null || this.checkLeft(left_obj))
            && (right_obj == null || this.checkRight(right_obj))
            );
    }
    /// <summary>
    /// Проверка левой стороны части комнаты
    /// </summary>
    /// <param name="check_obj"></param>
    /// <returns></returns>
    public virtual bool checkLeft(TowerRoomPartCheck check_obj)
    {
        return true;
    }
    /// <summary>
    /// Проверка правой стороны части комнаты
    /// </summary>
    /// <param name="check_obj"></param>
    /// <returns></returns>
    public virtual bool checkRight(TowerRoomPartCheck check_obj)
    {
        return true;
    }
    /// <summary>
    /// обёртка проверки типа обьекта, то есть 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="chek_obj"></param>
    /// <returns></returns>
    public bool checkType<T>(TowerRoomPartCheck chek_obj) {
        return (chek_obj is T);
    }
}
