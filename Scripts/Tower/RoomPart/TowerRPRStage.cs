﻿using UnityEngine;
using System.Collections;

public class TowerRPRStage : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPRStage.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPBStageLeft>(check_obj)
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPRStage.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPLWall>(check_obj)
            );
    }
}
