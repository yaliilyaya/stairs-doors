﻿using UnityEngine;
using System.Collections;

public class TowerRPLStage : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPLStage.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPRWall>(check_obj)
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPLStage.checkLeft " + check_obj.ToString());
        return (            
            this.checkType<TowerRPBStageRight>(check_obj)
            );
    }
}
