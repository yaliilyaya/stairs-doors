﻿using UnityEngine;
using System.Collections;

public class TowerRPLDoor : TowerRoomPartCheck
{
    public override bool checkLeft(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPLDoor.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPRDoor>(check_obj)
            );
    }

    public override bool checkRight(TowerRoomPartCheck check_obj)
    {
        //Debug.Log("TowerRPLDoor.checkLeft " + check_obj.ToString());
        return (
            this.checkType<TowerRPBFloor>(check_obj)
            );
    }
}
