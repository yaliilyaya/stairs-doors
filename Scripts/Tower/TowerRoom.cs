﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Обьект реализует методы и свойства каждой комнаты на сцене
/// </summary>
public class TowerRoom : MonoBehaviour {

    public Vector2 position;
    public int numberType = 0;

    protected TowerRoomPart part_left;
    protected TowerRoomPart part_right;
    protected TowerRoomPart part_bottom;
    
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    /// <summary>
    /// Задаёт номер комнаты и загружает нужные её части
    /// </summary>
    /// <param name="number">порядковый номер комнаты</param>
    public void setNumber(int number) {
        this.numberType = number;
        if (this.numberType > 0)
        {
            this.cteatePart(this.numberType);
        }
    }
    /// <summary>
    /// Задаёт позицию комнаты
    /// </summary>
    /// <param name="poss">Позиция в мировом пространстве</param>
    public void setPosition(Vector3 poss) {
        transform.position = poss;
        this.position = TowerGrid.getPossCell<Vector2>(poss);
    }
    /// <summary>
    /// Задаёт позицию комнаты
    /// </summary>
    /// <param name="poss">Позиция в сетке</param>
    public void setPosition(Vector2 poss)
    {
        this.position = poss;
        this.transform.position = TowerGrid.getPossCell(poss);
    }
    
    /// <summary>
    /// Возращает объект комнаты используя игровой объект
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static TowerRoom get(GameObject obj) {
        return obj.GetComponent<TowerRoom>();
    }

    /// <summary>
    /// Расчитывает номер комнаты по номерам частей комнаты
    /// </summary>
    /// <param name="left"></param>
    /// <param name="right"></param>
    /// <param name="bottom"></param>
    /// <returns></returns>
    public static int getNumber(int left, int right, int bottom)
    {
        return left * 12 + right * 3 + bottom;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public TowerRoomPart getPartLeft()
    {
        return this.part_left;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public TowerRoomPart getPartRight()
    {
        return this.part_right;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public TowerRoomPart getPartBottom()
    {
        return this.part_bottom;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public TowerRoomPart getPart(int type) {
        switch (type) {
            case TowerRoomPart.TYPE_BOTTOM: {
                    return this.part_bottom;
                }
            case TowerRoomPart.TYPE_LEFT: {
                    return this.part_left;
                }
            case TowerRoomPart.TYPE_RIGHT: {
                    return this.part_right;
                }
        }
        return null;
    }

    /// <summary>
    /// Создаёт комнату.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public static TowerRoom create(int x, int y, int number) {
        return TowerRoom.create(new Vector2(x, y), number);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public static TowerRoom create(Vector2 pos, int number)
    {
        GameObject obj = CAPI.getPrifab("RoomEmpty");
        TowerRoom room = TowerRoom.get(obj);
        obj.name = "TowerRoom" + number;

        room.setPosition(pos);        
        room.setNumber(number);

        return room;
    }
    /// <summary>
    /// Создаёт части комнаты
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    protected bool cteatePart(int number) {

        this.part_left = this.cteatePart(number / 12, TowerRoomPart.TYPE_LEFT);
        number %= 12;
        this.part_right = this.cteatePart(number / 3, TowerRoomPart.TYPE_RIGHT);
        this.part_bottom = this.cteatePart(number % 3, TowerRoomPart.TYPE_BOTTOM);

        return true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="number"></param>
    /// <param name="part_type"></param>
    /// <returns></returns>
    protected TowerRoomPart cteatePart(int number, int part_type) {
        TowerRoomPart part = TowerRoomPart.create(this, part_type, number);
           
        return part;
    }
    /// <summary>
    /// Осуществляет проверку на правила стыковки комнаты
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public bool check(Vector2 pos) {
        TowerRoom room_center = TowerMain.findRoom(pos);
        Vector2[] near_cell_list = TowerGrid.getNearCell(pos);
        TowerRoom[] near_room_list = TowerMain.findRoom(near_cell_list);
        
        List<TowerRoom> list_near_room = new List<TowerRoom>(near_room_list);

        if (room_center != null && room_center != this) {
            return false;
        }

        if (TowerMain.isWhotStructure(this)
            && this.check(list_near_room)
            && this.part_left.check(list_near_room) 
            && this.part_right.check(list_near_room)
            && this.part_bottom.check(list_near_room)
        ) {
            return true;
        }

        return false;
    }
    /// <summary>
    /// Основной метод для проверки правил стыковки
    /// </summary>
    /// <param name="list_near_room"></param>
    /// <returns></returns>
    public bool check(List<TowerRoom> list_near_room) {
        return list_near_room.Exists(x => x != null && x != this);
    }
}
