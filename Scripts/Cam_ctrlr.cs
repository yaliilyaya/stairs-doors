﻿using UnityEngine;
using System.Collections;

public class Cam_ctrlr : MonoBehaviour {
    public Plane map_plane;

    public static bool out_field = false;

    private Transform cursor;
    private bool drag = false;
    private GameObject dragged_room;
    private GameObject curr_cell;
    private int field_layer = 1 << 8;
    private int rooms_layer = 1 << 9;
    private Vector3 room_old_pos = Vector3.zero;
    private bool ortho = false;
	// Use this for initialization
	void Start () {
        map_plane = new Plane(Vector3.forward, -0.8f);
        GameObject cursor_go = new GameObject("Cursor");
        cursor = cursor_go.transform;
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float rayDistance;
        if (map_plane.Raycast(ray, out rayDistance))
        { 
            cursor.position = ray.GetPoint(rayDistance);
        }

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, rooms_layer))
        {
            if (drag == false) {
                if (Input.GetMouseButtonDown(0)) {
                    drag = true;
                    room_old_pos = hit.collider.transform.position;
                    dragged_room = hit.collider.gameObject;
                    dragged_room.layer = 0;
                }
            }
        }

        if (drag) {
            dragged_room.transform.position = cursor.position;

            if (Physics.Raycast(ray, out hit, 100, field_layer))
            {
                curr_cell = hit.collider.gameObject;
                out_field = false;
            }
            else
            {
                curr_cell = null;
                out_field = true;
            }

            if (!Physics.Raycast(ray, out hit, 100, rooms_layer))
            {
                if (curr_cell)
                {
                    curr_cell.GetComponent<Cell_ctrlr>().up_glow_blue = true;
                }
            }
            else
            {
                curr_cell.GetComponent<Cell_ctrlr>().up_glow_red = true;
            }

            if (Input.GetMouseButtonUp(0)) {
                if (!Physics.Raycast(ray, out hit, 100, rooms_layer))
                {
                    if (Physics.Raycast(ray, out hit, 100, field_layer))
                    {
                        dragged_room.transform.position = hit.collider.transform.position;
                        drag = false;
                    }
                    else
                    {
                        dragged_room.transform.position = room_old_pos;
                        drag = false;
                        out_field = false;
                    }
                }
                else
                {
                    dragged_room.transform.position = room_old_pos;
                    drag = false;
                }
                dragged_room.layer = 9;
            }
        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (!ortho) {
                Camera.main.orthographic = true;
                ortho = true;
            }
            else
            {
                Camera.main.orthographic = false;
                ortho = false;
            }
        }
	}

    void OnGUI()
    {
        GUI.Label(new Rect(10,10, 100,30), cursor.position.x.ToString());
        GUI.Label(new Rect(10, 40, 100, 30), cursor.position.y.ToString());
    }
}
