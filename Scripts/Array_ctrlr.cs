﻿using UnityEngine;
using System.Collections;

public class Array_ctrlr : MonoBehaviour {

    public GameObject obj_to_clone;
    public Vector3 offset = Vector3.zero;
    public Vector3 dimension = Vector3.zero;

    private int dim_x;
    private int dim_y;
    private int dim_z;
	// Use this for initialization
	void Start () {
        dim_x = (int)dimension.x;
        dim_y = (int)dimension.y;
        dim_z = (int)dimension.z;

        Vector3 start_pos = transform.position;

        for (int ix = 0; ix < dim_x; ix++)
        {
            for (int iy = 0; iy < dim_y; iy++)
            {
                for (int iz = 0; iz < dim_z; iz++)
                {
                    GameObject clone = Instantiate(obj_to_clone, new Vector3(start_pos.x + ix*offset.x, start_pos.y + iy*offset.y, start_pos.z + iz*offset.z), Quaternion.identity) as GameObject;
                } 
            } 
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
